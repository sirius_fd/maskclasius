from argparse import ArgumentParser
import os
from os import listdir
from os.path import isfile, isdir, join
import sys
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.imagenet_utils import preprocess_input, decode_predictions
import time
import fnmatch
import cv2

available_extensions = ['.png', '.jpg', '.bmp', '.ppm', '.tif']
def file_is_image(path):
  _, extension = os.path.splitext(path)

  return extension.lower() in available_extensions

def prep(img):
    image = cv2.resize(img, (300,300))
    input_arr = keras.preprocessing.image.img_to_array(np.asarray(image))
    input_arr = keras.applications.inception_v3.preprocess_input(np.array([input_arr]))
    return input_arr

def main():
  parser = ArgumentParser(description='Script for faces classification (in mask or without mask)')
  parser.add_argument(
      '--camera',
      default=0,
      type=int, 
      help='Id of video capture device')
  parser.add_argument(
      '--stdin',
      type=bool,
      help='Activates stdin for files input')
  parser.add_argument(
      '--model',
      type=str,
      default='./model',
      help='Specifies path to the model')

  args = parser.parse_args()


  if not os.path.exists(args.model):
    print('Path not found')
    sys.exit(1)

  model = keras.models.load_model(args.model)

  if args.stdin:
    stdin_process(model)
  else:
    camera = args.camera
    capture = cv2.VideoCapture(camera)
    video_process(capture, model)

def video_process(capture, model):
  font = cv2.FONT_HERSHEY_SIMPLEX
  capture.set(3, 640)
  capture.set(4, 480)
  last = time.time()
  model = keras.models.load_model('./model.hdf5')
  flag = True
  while True:
      ret, img = capture.read()
  
      gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
      if int(time.time() - last) >= 1:
        flag = model.predict(prep(img))[0] < 0.001
        last = time.time()
  
      cv2.putText(img, 'In mask' if flag else 'Without mask', (30, 30), font, 1, (0, 0, 255) if flag else (255, 0, 0), 3)
      cv2.imshow('image', img)
  
      if cv2.waitKey(100) == ord("q"):
          break
  capture.release()
  cv2.destroyAllWindows()
  
def stdin_process(model):
    while True:
        print('Input path:')
        source = input()
        if len(source) == 0:
            continue
        if not os.path.exists(source):
          print('File not found')
          continue
        
        if isfile(source):
          if not file_is_image(source):
            print('Invalid file format')
            continue
          in_mask = model.predict(prep(cv2.imread(source)))[0] < 0.5
          print('In mask' if in_mask else 'Without mask')
        elif isdir(source):
          tp = 0
          fn = 0
          tn = 0
          fp = 0
          files = [os.path.join(dp, f) for dp, dn, filenames in os.walk(source) for f in filenames if file_is_image(f)]
          for f in files:
            flag = model.predict(prep(cv2.imread(join(source, f))))[0] < 0.5
            if flag:
              if fnmatch.fnmatch(f, '*/msk/*'):
                tp += 1
              else:
                fp += 1
            else:
              if fnmatch.fnmatch(f, '*/nomsk/*'):
                tn += 1
              else:
                fn += 1
            print(f'{f}: {"In mask" if flag else "Without mask"}')
          print(f'FN: {fn}, FP: {fp}, TN: {tn}, TP: {tp}')
          precision = tp / (tp + fp)
          recall = tp / (fn + tp)
          f1 = 2 * (precision * recall)/(precision+recall)
          print(f'Results:\nPrecision: {precision}\nRecall: {recall}\nF1: {f1}')

if __name__ == "__main__":
  main()
