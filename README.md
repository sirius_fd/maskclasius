# Face detection

This project was done as part of the Sirius educational program.

You can download the pretrained model by the [link](https://cloud.spbpu.com/s/aEcpSs2gHiCjwnQ)

Every launch you must be connect model use arg `--model`:

```
python maskclasius.py --model <path/to/model.hdf5>
```

To launch soft run:
```
python maskclasius.py
```

By default it will process video stream from the first initialized camera in the system.

To choose other camera use arg `--camera`:

```
python maskclasius.py --camera=0 --model <path/to/model.hdf5>
```

You can also process image files or directories:

```
python maskclasius.py --stdin true
```

This options makes soft read path to file or directory from stdin and then process them in real time.

Precision, Recall and F1 metrics were calculated with these datasets:

* [ffhq-dataset](https://github.com/NVlabs/ffhq-dataset) 1000 images
* [RMFD](https://drive.google.com/drive/folders/1kZAIiv34Iav9Vt8BB101FXo4KoEClpx9) 1000 images

Result metrics:

* Precision: 0.9639830508474576
* Recall: 0.9257375381485249
* F1: 0.9444732745199792

