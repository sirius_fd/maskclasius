import setuptools

setuptools.setup(
    name="maskclasius",
    version="1.0.2",
    author="Kashirkiy Mark, Alexey Orazov, Misha Mamaev, Ildan Petrov, Ivan Loseev, Liana Kertieva",
    author_email="marshelo44@gmail.com, alordash@mail.ru, mic.mamaev@yandex.ru, ildan.petrov@gmail.com, loseev5@gmail.com, Kertieva.liana@rdebc.ru",
    url="https://gitlab.com/sirius_fd/face_classification",
    description="Tool for classification faces on a frame from videostream or images",
    packages=setuptools.find_packages(),
    entry_points = {
        'console_scripts': [
            'maskclasius = src.maskclasius.__main__:main',
        ],
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
    install_requires=[
      'numpy',
      'tensorflow>=2.4.0',
      'opencv-python>=4.2.0.34',
    ]
)
